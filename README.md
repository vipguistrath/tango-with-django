# Quickstart #

* Run pip3 install -U django==1.7
* Run pip3 install django-registration-redux

* Clone the repository
* Run python3 manage.py migrate
* Run python3 manage.py createsuperuser, and follow instructions
* Run python3 manage.py runserver
* Go to http://127.0.0.1:8000/admin on a web browser, and log in
